package com.middleware.shift;

import com.middleware.util.CRCUtils;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Created by s.he
 * on 2017\8\22 0022.
 */
public class JavaShift {
    private static final Logger logger = Logger.getLogger(JavaShift.class);

    @Test
    public void test1() {
        int number = 10;
        int number1 = number << 2;
        logger.info(number1);
        logger.info(Integer.toBinaryString(number));
        logger.info(Integer.toBinaryString(number1));
    }

    @Test
    public void test2() {
        int number = -9;
        int number1 = number << 2;

        logger.info(number1);
        logger.info(Integer.toBinaryString(number));
        logger.info(Integer.toBinaryString(number1));
    }

    @Test
    public void test3() {
        int number = -9;
        int number1 = number >> 2;

        logger.info(number1);
        logger.info(Integer.toBinaryString(number));
        logger.info(Integer.toBinaryString(number1));
    }

    @Test
    public void test4() {
        int number2 = 9;
        int number1 = number2 >> 2;

        logger.info(number1);
        logger.info(Integer.toBinaryString(number2));
        logger.info(Integer.toBinaryString(number1));
    }

    @Test
    public void test5() {
        int number2 = -9;
        int number1 = number2 >>> 2;

        logger.info(number1);
        logger.info(Integer.toBinaryString(number2));
        logger.info(Integer.toBinaryString(number1));
    }

    @Test
    public void test6() {
        int number = 070;
        logger.info(number);
        logger.info(Integer.toBinaryString(number));

        number = number << 2;
        logger.info(number);
        logger.info(Integer.toBinaryString(number));
    }

    @Test
    public void test7() {
        int number = 0XFF;
        logger.info(number);
        logger.info(Integer.toBinaryString(number));
    }

    @Test
    public void test8() throws Exception {
           String message = "78,78,11,01,08,68,12,01,48,37,35,71,36,05,32,02,00,39,DE,F7,0D,0A";

       String [] messages = message.split(",");
        byte[] result = new byte[messages.length];
       for(int i=0; i<messages.length; i++) {
            result[i] = get2From16(messages[i]);
       }


      /* StringBuilder r = new StringBuilder();
       StringBuilder m = new StringBuilder();
       for(byte b : result) {
           r.append(get8BitString(Integer.toBinaryString(Byte.toUnsignedInt(b))));
           m.append(get8BitStringOf16(Integer.toHexString(Byte.toUnsignedInt(b))));
       }*/
    }

    @Test
    public void test12() throws Exception {
        String message = "11,01,08,68,12,01,48,37,35,71,36,05,32,02,00,39";
        String [] messages = message.split(",");
        byte[] result = new byte[messages.length];
        for(int i=0; i<messages.length; i++) {
            result[i] = get2From16(messages[i]);
        }

        StringBuilder r = new StringBuilder();
        StringBuilder m = new StringBuilder();
        for(byte b : result) {
            r.append(get8BitString(Integer.toBinaryString(Byte.toUnsignedInt(b))));
            m.append(get8BitStringOf16(Integer.toHexString(Byte.toUnsignedInt(b))));
        }
        System.out.println("hsd................." + result.length);
        System.out.println(m.toString());

        short t = CRCUtils.crcAuth(result);
        System.out.println(t);
    }

    @Test
    public void test14() {
        byte[] sequenceByte = new byte[2];
        sequenceByte[0] = (byte) 0XDE;
        sequenceByte[1] = (byte) 0XF7;

        int sequence = ((sequenceByte[0] << 8) & 0X0000FF00) + (sequenceByte[1] & 0X000000FF);
        logger.info((short)sequence);
    }

    @Test
    public void test13() {
        byte[] hello = "123456789".getBytes();
        byte[] world = "abcdefg".getBytes();
        byte helloWorld[][] = new byte[][]{hello, world};
        for(byte[] t : helloWorld) {
            for(byte m : t) {
                System.out.println(m);
            }
        }

    }

    @Test
    public void test9() {
        int result = Integer.valueOf("DE", 16);
        logger.info(Integer.toBinaryString(result));
    }

    private byte get2From16(String data) {
        int result = Integer.valueOf(data, 16);
        return (byte)result;
    }

    @Test
    public void test10() {
        byte b = (byte)0XFF;
        logger.info(Integer.toBinaryString(Byte.toUnsignedInt(b)));
    }

    private String get8BitString(String data) {
        StringBuilder result = new StringBuilder("");
        int length = data.length();
        for(int i=0; i<8-length; i++) {
            result.append("0");
        }
        return result.append(data).toString();
    }

    private String get8BitStringOf16(String data) {
        StringBuilder result = new StringBuilder("");
        int length = data.length();
        for(int i=0; i<2-length; i++) {
            result.append("0");
        }
        return result.append(data).toString();
    }

    @Test
    public void testImie() throws Exception {
        String imei = "01234567891234DF";
        int i = 0;
        byte[] result = new byte[8];
        int count = 0;
        while (i < imei.length()) {
            result[count] = Integer.valueOf(imei.substring(i, i+2), 16).byteValue();
            i = i+2;
            count = count + 1;
        }
       String s =  print(result);
        System.out.println(s);

    }

    private String print(byte[] result) {
        StringBuilder s = new StringBuilder();
        for(byte b : result) {
            int m = Byte.toUnsignedInt(b);
            s.append(get8BitStringOf16(Integer.toHexString(m)));

        }
        return s.substring(1).toUpperCase();
    }

    private String getImie(byte[] data) {
        StringBuilder result = new StringBuilder();
        for (int i=0; i<data.length; i++) {
            int value = (data[i] >> 4) & 0X0F;
            String value16 = Integer.toHexString(value);
            result.append(value16);

            value = data[i] & 0X0F;
            value16 = Integer.toHexString(value);
            result.append(value16);
        }

        return result.substring(1).toUpperCase();
    }
}
