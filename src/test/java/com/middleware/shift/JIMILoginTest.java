package com.middleware.shift;

import org.junit.Test;

/**
 * Created by s.he
 * on 2017\8\30 0030.
 */
public class JIMILoginTest {
    @Test
    public void testLogin() {
        String message = "78 78 11 01 08 68 12 01 62 70 57 32 36 05 32 02 00 08 CB 47 0D 0A";
        ByteUtils.getMessageFromServer(message);
    }

    @Test
    public void testReply() {
        String message = "78 78 0C 01 11 08 12 00 21 20 00 00 08 B4 65 0D 0A";
        String msg[] = message.split(" ");
        for(int i=0; i<msg.length; i++) {
            String h = msg[i];
            System.out.println(i + ":" + ByteUtils.get2From16(h));
        }
    }


}
