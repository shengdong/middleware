package com.middleware.shift;

import com.middleware.socket.SocketClient;

/**
 * Created by s.he
 * on 2017\8\31 0031.
 */
public class ByteUtils {

    public static byte[] getMessageFromServer(String message) {
        String [] messages = message.split(" ");
        byte[] result = new byte[messages.length];
        for(int i=0; i<messages.length; i++) {
            result[i] = get2From16(messages[i]);
        }
        return result;
       /* StringBuffer stringBuffer = new StringBuffer("");
        for(int i=0; i<result.length; i++) {
            stringBuffer.append(result[i] + " ");
        }
        System.out.println(stringBuffer.toString());
        byte[] data= SocketClient.sendToServer("13.114.244.159", 29121, 6000, result);
        return data;*/
    }

    public static byte get2From16(String data) {
        int result = Integer.valueOf(data, 16);
        return (byte)result;
    }


}
