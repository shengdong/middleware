package com.middleware.shift;

import com.mongodb.util.JSON;
import org.junit.Test;

/**
 * Created by s.he
 * on 2017\8\28 0028.
 */
public class Test1 {

    @Test
    public void test1() {
        int m = getByte() & 0X00FF;
        System.out.println(m);
    }

    @Test
    public void test2() {
        double m = 2.35355d;
        System.out.println(0 - m);
    }

    @Test
    public void test3() {
        String m = "UNLOCK";
        System.out.println(m.getBytes());
    }

    private byte getByte() {
        return (byte)0XA5;
    }
}
