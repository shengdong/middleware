package com.middleware.shift;

import org.junit.Test;

/**
 * Created by s.he
 * on 2017\8\29 0029.
 */
public class JIMIPositionTest {

    @Test
    public void testPosition() throws Exception {
        String message = "79 79 00 3E 32 11 03 14 09 06 08 00 09 01 CC 00 28 7D 00 1F 40 0E 24 28 7D 00 1F 71 07 28 7D 00 1E 3F 06 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 08 84 44 0D 0A";
        ByteUtils.getMessageFromServer(message);
    }

    @Test
    public void t1() {
        int m = 655365;
        byte t = (byte) (m >> 24 & 0XFF);
        System.out.println(Integer.toBinaryString(m));
        System.out.println(t);
    }

    @Test
    public void t2() {
        String message = "47 42 31 30 30 5F 31 30 5F 41 31 44 5F 44 32 33 5F 52 30 5F 56 30 38 5F 57 4D";
        byte[] msg = ByteUtils.getMessageFromServer(message);
        System.out.println(new String(msg));
    }

}
