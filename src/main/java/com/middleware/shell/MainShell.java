package com.middleware.shell;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

public class MainShell {
	private static final Logger logger = Logger.getLogger(MainShell.class);
	
	
	public static void main(String[] args) {
		MainShell shell = new MainShell();
		shell.executeSehll("/home/hsd/soft/tomcat/bin/shutdown.sh");
	}
	
	
	private boolean executeSehll(String path) {
		String cmdString = "bash " + path;
		try {
			Process process = Runtime.getRuntime().exec(cmdString);
			 String ls_1 = "";
			 BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			 while ( (ls_1 = bufferedReader.readLine()) != null);
			 bufferedReader.close();
			 process.waitFor();
			 logger.info(ls_1);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return true;
	}
}
