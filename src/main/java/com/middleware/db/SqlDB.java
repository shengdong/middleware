package com.middleware.db;

import java.sql.Connection;

public interface SqlDB {

	public Connection getConnection();
}
