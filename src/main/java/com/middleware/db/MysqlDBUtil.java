package com.middleware.db;

import java.sql.SQLException;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.log4j.Logger;

public class MysqlDBUtil {
	private static final Logger logger = Logger.getLogger(MysqlDBUtil.class);
	
	private BasicDataSource dataSource;
	
	public MysqlDBUtil() {
		dataSource = new MysqlDBCP().getDataSource();
	}
	
	public void insertDB() {
		try {
			QueryRunner runner = new QueryRunner(dataSource);
			String sql = "insert into Company(id, name, des, address) values(?,?,?,?)";
			Object params[] = {"5", "youku", "a movie of youku", "www.youku.com"};
			
			int i = runner.update(sql, params);
			logger.info("i" + i);
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
