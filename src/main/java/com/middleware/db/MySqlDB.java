package com.middleware.db;

import java.sql.Connection;
import java.sql.DriverManager;
import com.middleware.util.ConfigUtil;

public class MySqlDB implements SqlDB{
	
	private static String username = "";
	
	private static String password = "";
	
	private static String className = "";
	
	private static String url = "";
	
	static {
		username = ConfigUtil.getString("mysql.username");
		password = ConfigUtil.getString("mysql.password");
		className = ConfigUtil.getString("mysql.className");
		url = ConfigUtil.getString("mysql.url");
	}
	
	public Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName(className);
			connection = DriverManager.getConnection(url, username, password);

			return connection;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String[] args) {
		System.out.println(new MySqlDB().getConnection());
	}
}
