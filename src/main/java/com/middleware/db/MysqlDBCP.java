package com.middleware.db;


import java.sql.SQLException;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

import com.middleware.util.ConfigUtil;

public class MysqlDBCP {
	private static final Logger logger = Logger.getLogger(MySqlDB.class);
	
	private BasicDataSource source = null;
	
	public MysqlDBCP() {
		source = new BasicDataSource();
		source.setUrl(ConfigUtil.getString("mysql.url"));
		source.setDriverClassName(ConfigUtil.getString("mysql.className"));
		source.setUsername(ConfigUtil.getString("mysql.username"));
		source.setPassword(ConfigUtil.getString("mysql.password"));
		source.setTestWhileIdle(true);
		source.setTimeBetweenEvictionRunsMillis(30000);
		source.setMinEvictableIdleTimeMillis(1800000);
		source.setMinIdle(5);
		source.setMaxActive(10);
		source.setInitialSize(5);
		source.setMaxIdle(10);
		source.setMaxWait(30000);
		try {
			logger.info(source.getConnection());
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	public BasicDataSource getDataSource() {
		return source;
	}

}
