package com.middleware.db;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * create by s.he
 * on 2017/11/23 0023
 */
public class DataHandler {

    public List<County> getData(String path) throws Exception{
        List<County> result  = new ArrayList<County>();
        InputStream stream = new FileInputStream(path);
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] split = line.split(",");
            County county = new County(split[0], split[1], split[2]);
            result.add(county);
        }

        reader.close();
        stream.close();

        return result;
    }
}
