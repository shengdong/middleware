package com.middleware.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

/**
 * create by s.he
 * on 2017/11/23 0023
 */
public class Main {
    public static void main(String[] args) throws Exception{
        String path = "D:\\code\\heshegndong\\数据\\县名称和编号.txt";

        DataHandler handler = new DataHandler();
        List<County> data = handler.getData(path);

        String sql = "insert into county(code, name, province_code) values(?,?,?)";
        Connection connection = new MySqlDB().getConnection();
        connection.setAutoCommit(false);
        PreparedStatement statement = connection.prepareStatement(sql);
        int count = 0;
        for(County county : data) {
            statement.setString(1, county.getCode());
            statement.setString(2, county.getName());
            statement.setString(3, county.getProvinceCode());
            statement.addBatch();
            count ++;
            if(count == 1000) {
                statement.executeBatch();
                connection.commit();
                count = 0;
            }
        }
        statement.executeBatch();
        connection.commit();

        statement.close();
        connection.close();

    }
}
