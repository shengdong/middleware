package com.middleware.socket;


import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * create by s.he
 * on 2017/12/9 0009
 */
public class SocketServerHsd {

    public static void main(String[] args) throws Exception{
        ServerSocket serverSocket = new ServerSocket(8989);
        while (true) {
            Socket accept = serverSocket.accept();
            accept.setKeepAlive(false);
            InputStream inputStream = accept.getInputStream();
            print(inputStream);
        }

    }

    private static void print(InputStream inputStream) throws Exception{
        byte[] data = new byte[1024];
        inputStream.read(data);
        System.out.println(new String(data).trim());
    }
}
