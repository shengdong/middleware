package com.middleware.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.apache.log4j.Logger;

import com.middleware.util.ConfigUtil;
import org.apache.log4j.net.SocketServer;

/**
 * @author k1193
 */
public class SocketClient {
	private static final Logger logger = Logger.getLogger(SocketClient.class);
    /**
     * declare logger
     */

    /**
     * send to server
     *
     * @param ip ip
     * @param port port
     * @param timeout  timeout
     * @param message message
     * @return message
     */
    public static byte[] sendToServer(String ip, int port, int timeout, byte[] message) {
        Socket socket = null;

        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            socket = new Socket(ip, port);
            socket.setSoTimeout(timeout);
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            outputStream.write(message);
            outputStream.flush();

            byte[] receivedMsg = new byte[1024];

            int count = inputStream.read(receivedMsg, 0, receivedMsg.length);
            byte[] result = new byte[count];
            System.arraycopy(receivedMsg, 0, result, 0, result.length);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static String getMessage(String message) {
        return message + ConfigUtil.getString("server.delimiter");
    }
}
