package com.middleware.queue;

import java.util.Stack;

public class StacksTest {

	public static void main(String[] args) {
		Stack<String> stack = new Stack<String>();
		stack.push("hello");
		stack.push("hello1");
		stack.push("hello2");
		stack.push("hello3");
		stack.push("hello4");
		
		System.out.println(stack.get(0));
		stack.remove(0);
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
	}
}
