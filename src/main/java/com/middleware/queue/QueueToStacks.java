package com.middleware.queue;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class QueueToStacks {
	private static final Logger logger = Logger.getLogger(QueueToStacks.class);
	
	private List<String> list = new ArrayList<String>();
	
	private static int count = -1;
	
	
	public boolean push(String ... names) {
		for(String name : names) {
			list.add(name);
			count ++;
		}
		return false;
	}
	
	public String pop() {
		if(count < 0) {
			return "";
		} else {
			String result =  list.get(count);
			list.remove(result);
			count --;
			return result;
		}
	}
	
	public static void main(String[] args) {
		QueueToStacks shed = new QueueToStacks();
		shed.push("s.he", "s.wang", "mengjie");
		logger.info(shed.pop());
		shed.push("s.he1", "s.wang1", "mengjie1");
		logger.info(shed.pop());
		shed.push("s.he2", "s.wang2", "mengjie2");
	}
}

