package com.middleware.annotation;

/**
 * Created by Idea
 * date: 2017/7/22 0022.
 * time: 11:25.
 * user: s.he
 */
public class Orange extends Base {

    @FruitName(value="北京 orange")
    public String fruitName;

    @FruitColor(fruitColor= FruitColor.Color.YELLOW)
    public String fruitColor;

    @FruitProvider(id=2,user="s.he",address="China")
    public FruitProvider provider;
}
