package com.middleware.annotation;

import com.middleware.inject.InjectUtil;

import java.lang.reflect.Field;

/**
 * Created by Idea
 * date: 2017/7/22 0022.
 * time: 10:46.
 * user: s.he
 */
public class Test {

    public static void main(String[] args) {
        Orange orange = new Orange();
        System.out.println("Test.main : " + orange.fruitColor);
        System.out.println("Test.main : " + orange.fruitName);
        System.out.println("Test.main : " + orange.provider.user());
        System.out.println("Test.main : " + orange.provider.address());
        System.out.println("Test.main : " + orange.provider.id());
    }
}
