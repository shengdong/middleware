package com.middleware.annotation;

import com.middleware.inject.InjectUtil;
import java.lang.reflect.Field;

/**
 * Created by Idea
 * date: 2017/7/22 0022.
 * time: 11:18.
 * user: s.he
 */
public class Method {
    public static void getFruitInfo(Object obj) {
        try {
            Class<?> cls = obj.getClass();
            Field[] fields = cls.getDeclaredFields();

            for(Field field : fields) {
                if(field.isAnnotationPresent(FruitName.class) == true) {
                    FruitName name = field.getAnnotation(FruitName.class);

                    InjectUtil.injectValue(field.getName(), name.value(), obj);
                }

                if(field.isAnnotationPresent(FruitColor.class)){
                    FruitColor color = field.getAnnotation(FruitColor.class);
                    InjectUtil.injectValue(field.getName(), color.fruitColor().name(), obj);
                }
                if(field.isAnnotationPresent(FruitProvider.class)) {
                    FruitProvider provider = field.getAnnotation(FruitProvider.class);
                    InjectUtil.injectValue(field.getName(), provider, obj);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
