package com.middleware.annotation;

/**
 * Created by Idea
 * date: 2017/7/22 0022.
 * time: 10:45.
 * user: s.he
 */
public class Apple extends Base {

    @FruitName(value = "北京 Apple")
    public String fruitName;

    @FruitColor(fruitColor = FruitColor.Color.RED)
    public String fruitColor;

    @FruitProvider(id = 1, user = "Tom", address = "China")
    public FruitProvider provider;




}
