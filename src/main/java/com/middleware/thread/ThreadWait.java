package com.middleware.thread;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.CountDownLatch;
import org.apache.log4j.Logger;

public class ThreadWait {
	private static final Logger logger = Logger.getLogger(ThreadWait.class);
	
	public static Object LOCK = new Object();
	
	public static int i = 0;
	
	
	public static void main(String[] args) {
		String filePath = "/home/hsd/code/middleware/src/main/java/com/middleware/springclient";
		File file = new File(filePath);
		if(file.listFiles() != null) {
			new ThreadWait().execute(file.listFiles());
		}
	}
	
	public void execute(File[] files) {
		final int threadNumber = files.length;
		final CountDownLatch countDownLatch = new CountDownLatch(threadNumber);
		logger.error("hsd....start");
		for(File file : files) {
			new Thread(new PrintClass(countDownLatch, file.getAbsolutePath())).start();
		}
		try {
			countDownLatch.await();
			logger.error("hsd....end : " + i);
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		}
	}
}

class PrintClass implements Runnable {
	private static final Logger logger = Logger.getLogger(PrintClass.class);
	
	private CountDownLatch countDownLatch = null;
	
	private String filePath = "";
	
	
	public PrintClass(CountDownLatch countDownLatch, String filePath) {
		this.countDownLatch = countDownLatch;
		this.filePath = filePath;
	}

	public void run() {
		InputStream stream = null;
		BufferedReader reader = null;
		File file = new File(filePath);
		int count = 0;
		try {
			stream = new FileInputStream(filePath);
			reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
			while(reader.readLine() != null) {
				count ++;
			}
			
			synchronized (ThreadWait.LOCK) {
				ThreadWait.i = ThreadWait.i + 1;
			}
			logger.info("file : " + file.getName() +  ", count:" + count);
			countDownLatch.countDown();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			synchronized (ThreadWait.LOCK) {
				ThreadWait.i = ThreadWait.i  +  0;
			}
		}
	}
}
