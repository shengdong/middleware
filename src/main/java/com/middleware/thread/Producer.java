package com.middleware.thread;

import java.util.UUID;

import org.apache.log4j.Logger;

public class Producer {
	private static final Logger logger = Logger.getLogger(Producer.class);

	public static void main(String[] args) {
		new Thread(new Runnable() {
			
			int count = 0;
			@Override
			public void run() {
				while(true) {
					String data = UUID.randomUUID().toString();
					DataService.queue.add(data);
					logger.info(data + " : " + count++);
				}
			}
		}).start();
	}
}
