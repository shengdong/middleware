package com.middleware.exceutor;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ExecutorHandler {
	
	public static void main(String[] args) {
		ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
		Runnable runnable = new FileJob(service);
		service.scheduleAtFixedRate(runnable, 0, 5, TimeUnit.SECONDS);
	}
	
}

class FileJob implements Runnable {
	private ScheduledExecutorService service = null;
	
	private int count = 0;

	public FileJob(ScheduledExecutorService service) {
		System.out.println("constrcut.........");
		this.service = service;
	}

	public void run() {
		count ++ ;
		System.out.println("hsd....");
		if(count > 10) {
			service.shutdownNow();
			System.out.println("executing..............");
		}
	}

}