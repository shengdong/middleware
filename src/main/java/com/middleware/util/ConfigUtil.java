package com.middleware.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.log4j.Logger;

public class ConfigUtil {
	private static final Logger logger = Logger.getLogger(ConfigUtil.class);

	private static final String FILEPATH = "config.ini";
	
	private static HierarchicalINIConfiguration config = null;
	
	private synchronized static void readConfig() {
		try {
			config = new HierarchicalINIConfiguration(FILEPATH);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	public static String getString(String key) {
		if(config == null) {
			readConfig();
		}
		
		return config.getString(key);
	}
	
	public static int getInt(String key) {
		if(config == null) {
			readConfig();
		}
		
		return config.getInt(key);
	}
	
	public static String getToday(String pattern) {
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		Date date = new Date();
		return format.format(date);
	}

}
