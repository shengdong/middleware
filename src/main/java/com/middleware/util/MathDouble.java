package com.middleware.util;

import org.junit.Test;

public class MathDouble {
    private Double v1;

    private String v2;


    public Double getV1() {
        return v1;
    }

    public void setV1(Double v1) {
        this.v1 = v1;
    }

    public String getV2() {
        return v2;
    }

    public void setV2(String v2) {
        this.v2 = v2;
    }
}
