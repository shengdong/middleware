package com.middleware.util;

import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.log4j.Logger;

public class FileUtil {
	private static final Logger logger = Logger.getLogger(FileUtil.class);

	public String getMessage(String path) {
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(path);
			StringBuilder stringBuilder = new StringBuilder();
			byte[] data = new byte[1024];
			int length = 0;
			while ((length = inputStream.read(data)) > -1) {
				String message = new String(data, 0, length).trim();
				stringBuilder.append(message);
			}

			String message = stringBuilder.toString();

			return message;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return "";
		} finally {
			try {
				if(inputStream != null) {
					inputStream.close();
				}
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
}
