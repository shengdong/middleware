package com.middleware.util;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.apache.log4j.Logger;

import java.nio.charset.Charset;

/**
 * Created by s.he
 * on 2017\8\24 0024.
 */
public class CRCUtils {
    private static final Logger logger = Logger.getLogger(CRCUtils.class);

    public CRCUtils() {

    }

    public static short crcAuth(byte[] crcData) {
        byte[] key1 = Crc16Utils.keyStr1.getBytes(Charset.forName("US-ASCII"));
        byte [] data = new byte[crcData.length + key1.length];
        System.arraycopy(crcData, 0, data, 0, crcData.length);
        System.arraycopy(key1, 0, data, crcData.length, key1.length);
        Short calCrc = Crc16Utils.crc16(data);
        return calCrc.shortValue();
    }
}
