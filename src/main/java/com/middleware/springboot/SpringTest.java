package com.middleware.springboot;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by s.he
 * on 2017\8\21 0021.
 */
public class SpringTest {
    public static void main(String[] args) {
        BeanFactory beanFactory = new ClassPathXmlApplicationContext("1.xml");
        beanFactory.getBean("");
    }
}
