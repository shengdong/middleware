package com.middleware.redis;


import redis.clients.jedis.Jedis;
import redis.clients.jedis.ShardedJedis;

import java.util.Set;

/**
 * create by s.he
 * on 2017/12/5 0005
 */
public class BatchDeleteRedisKey {
    public static void main(String[] args) {
        Jedis jedis = new Jedis("192.168.5.64", 6379);

        jedis.auth("foobared");
        Set<String> keys = jedis.keys("CellTower_Group*");

        for(String key : keys) {
            jedis.del(key);
        }

    }
}
