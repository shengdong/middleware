package com.middleware.redis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.middleware.util.ConfigUtil;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;


public class JedisClusterUtil implements JedisUtil {
	private static final Logger logger = Logger.getLogger(JedisClusterUtil.class);

	private JedisCluster jedisCluster = null;

	private JedisClusterStsingUtil stsingUtil = null;

	
	public JedisClusterUtil() {
		Set<HostAndPort> jedisClusterNodes = new HashSet<HostAndPort>();

		String ips[] = ConfigUtil.getString("redis.host").split(";");

		for (String ip : ips) {
			String host = ip.split(":")[0].trim();
			String portString = ip.split(":")[1].trim();
			int port = Integer.parseInt(portString);
			HostAndPort hostAndPort = new HostAndPort(host, port);
			jedisClusterNodes.add(hostAndPort);
		}

		jedisCluster = new JedisCluster(jedisClusterNodes);
		stsingUtil = new JedisClusterStsingUtil();
		
		logger.error("jedisCluster number : " + jedisCluster.getClusterNodes().size());
	}
	
	public JedisCluster getJedisCluster() {
		return jedisCluster;
	}

	public void close() {
		try {
			jedisCluster.close();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public Map<String, String> hgetAll(String key) {
		return jedisCluster.hgetAll(key);
	}

	public String hmset(String key, Map<String, String> map) {
		return jedisCluster.hmset(key, map);
	}

	public Set<String> keys(String pattern) {
		return stsingUtil.keys(pattern, jedisCluster);
	}

	public boolean ping() {
		try {
			jedisCluster.set("check", "ok");
			jedisCluster.del("check");
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, String>> piplined(Set<String> keys) {
		List<Map<String, String>> result = new ArrayList<Map<String,String>>();
		JedisClusterStsingUtil jcp = JedisClusterStsingUtil.pipelined(jedisCluster);
		jcp.refreshCluster();
		List<Object> batchResult = null;
		try {
			for (String key : keys) {
				jcp.hgetAll(key);
			}
			
			batchResult = jcp.syncAndReturnAll();
			for(Object object : batchResult) {
				result.add((Map<String, String>)object);
			}
			jcp.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}

	public Set<String> smembers(String key) {
		return jedisCluster.smembers(key);
	}

	public String set(String key, String value) {
		return jedisCluster.set(key, value);
	}
}
