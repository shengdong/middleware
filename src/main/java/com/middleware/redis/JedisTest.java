package com.middleware.redis;

import java.lang.reflect.Field;

import redis.clients.jedis.BinaryJedisCluster;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisClusterConnectionHandler;
import redis.clients.jedis.JedisClusterInfoCache;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisSlotBasedConnectionHandler;
import redis.clients.util.JedisClusterCRC16;

public class JedisTest {

	public static void main(String[] args) {
		JedisClusterUtil jedisUtil = new JedisClusterUtil();
		JedisCluster cluster = jedisUtil.getJedisCluster();

		Field field = getField(BinaryJedisCluster.class, "connectionHandler");
		JedisSlotBasedConnectionHandler connectionHandler = getValue(cluster, field);
		JedisClusterInfoCache clusterInfoCache = getValue(connectionHandler,
				getField(JedisClusterConnectionHandler.class, "cache"));

		/**
		 * 
		 * e2 e6 ec f3 f7 fb
		 */

		int slotCode = JedisClusterCRC16.getSlot("ec");
		System.out.println(slotCode);

		int hashCode = JedisClusterCRC16.getCRC16("ec");
		System.out.println(hashCode % 16384);

		JedisPool jedisPool = clusterInfoCache.getSlotPool(slotCode);
		Jedis jedis = jedisPool.getResource();
		System.out.println(jedis.getClient().getHost());
		System.out.println(jedis.getClient().getPort());
	}

	@SuppressWarnings("unchecked")
	private static <T> T getValue(Object obj, Field field) {
		try {
			return (T) field.get(obj);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static Field getField(Class<?> cls, String fieldName) {
		try {
			Field field = cls.getDeclaredField(fieldName);
			field.setAccessible(true);

			return field;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
