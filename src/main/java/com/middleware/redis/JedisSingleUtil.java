package com.middleware.redis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.middleware.util.ConfigUtil;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;



/**
 * 
 * @author k1193
 *
 */
public class JedisSingleUtil implements JedisUtil {
	private Jedis jedis = null;
	
	
	public JedisSingleUtil() {
		String ip = ConfigUtil.getString("redis.ip");
		int port = ConfigUtil.getInt("redis.port");
		jedis = new Jedis(ip, port);
	}
	

	public void close() {
		jedis.close();
		
	}

	public Map<String, String> hgetAll(String key) {
		return jedis.hgetAll(key);
	}

	public String hmset(String key, Map<String, String> map) {
		return jedis.hmset(key, map);
		
	}

	public Set<String> keys(String pattren) {
		return jedis.keys(pattren);
	}

	public boolean ping() {
		String result = jedis.ping();
		if(StringUtils.equals(result, "PONG")) {
			return true;
		}
		return false;
	}

	public List<Map<String, String>> piplined(Set<String> keys) {
		List<Map<String, String>> resilt = new ArrayList<Map<String,String>>();;
		List<Response<Map<String, String>>> rsp = new ArrayList<Response<Map<String, String>>>(
				keys.size());
		Pipeline pipeline = jedis.pipelined();
		for (String key : keys) {
			rsp.add(pipeline.hgetAll(key));
		}
		pipeline.sync();
		for (Response<Map<String, String>> rs : rsp) {
			resilt.add(rs.get());
		}
		return resilt;
	}

	public Set<String> smembers(String key) {
		return jedis.smembers(key);
	}


	public String set(String key, String value) {
		return jedis.set(key, value);
	}
	
}
