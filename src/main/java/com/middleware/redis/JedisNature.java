package com.middleware.redis;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;

public class JedisNature {
	private static final Logger logger = Logger.getLogger(JedisNature.class);
	
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Jedis jedis = new Jedis("127.0.0.1", 6379);
		
		logger.info("start.......");
		Set<String> keys = jedis.keys("*");
		List<String> resilt = new ArrayList<String>();;
		List<Response<String>> rsp = new ArrayList<Response<String>>(
				keys.size());
		Pipeline pipeline = jedis.pipelined();
		for (String key : keys) {
			rsp.add(pipeline.get(key));
		}
		pipeline.sync();
		for (Response<String> rs : rsp) {
			resilt.add(rs.get());
		}
		logger.info("end......." + resilt.size());
	}
	
}
