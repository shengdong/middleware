package com.middleware.redis;

import java.io.Closeable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import redis.clients.jedis.BinaryJedisCluster;
import redis.clients.jedis.Client;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisClusterConnectionHandler;
import redis.clients.jedis.JedisClusterInfoCache;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisSlotBasedConnectionHandler;
import redis.clients.jedis.PipelineBase;
import redis.clients.jedis.exceptions.JedisMovedDataException;
import redis.clients.jedis.exceptions.JedisRedirectionException;
import redis.clients.util.JedisClusterCRC16;
import redis.clients.util.SafeEncoder;

public class JedisClusterStsingUtil extends PipelineBase implements Closeable{
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(JedisClusterStsingUtil.class);

	/**
     * 部分字段没有对应的获取方法，只能采用反射来做
     * 你也可以去继承JedisCluster和JedisSlotBasedConnectionHandler来提供访问接口
     */
	private static final Field FIELD_CONNECTION_HANDLER;

	/**
     * 部分字段没有对应的获取方法，只能采用反射来做
     * 你也可以去继承JedisCluster和JedisSlotBasedConnectionHandler来提供访问接口
     */
	private static final Field FIELD_CACHE;

	/**
     * 初始化FIELD_CONNECTION_HANDLER,FIELD_CACHE
     */
	static {
		FIELD_CONNECTION_HANDLER = getField(BinaryJedisCluster.class,
				"connectionHandler");
		FIELD_CACHE = getField(JedisClusterConnectionHandler.class, "cache");
	}

	/**
     * 	declare  connectionHandler
     */
	private JedisSlotBasedConnectionHandler connectionHandler;

	/**
     * declare clusterInfoCache
     */
	private JedisClusterInfoCache clusterInfoCache;

	/**
     * 根据顺序存储每个命令对应的Client
     */
	private Queue<Client> clients = new LinkedList<Client>(); 

	/**
     *  用于缓存连接
     */
	private Map<JedisPool, Jedis> jedisMap = new HashMap<JedisPool, Jedis>(); 

	/**
     * 是否有数据在缓存区
     */
	private boolean hasDataInBuf = false;

	/**
	 * 根据jedisCluster实例生成对应的JedisClusterPipeline
	 * 
	 * @param jedisCluster jedisCluster对象 
	 * @return 返回pipeline
	 */
	public static JedisClusterStsingUtil pipelined(JedisCluster jedisCluster) {
		JedisClusterStsingUtil pipeline = new JedisClusterStsingUtil();
		pipeline.setJedisCluster(jedisCluster);
		return pipeline;
	}

	/**
     * 构造方法
     */
	public JedisClusterStsingUtil() {
		
	}

	/**
	 * 
	 * @param jedisCluster 当前jedisCluster对象
	 */
	public void setJedisCluster(JedisCluster jedisCluster) {
		connectionHandler = getValue(jedisCluster, FIELD_CONNECTION_HANDLER);
		clusterInfoCache = getValue(connectionHandler, FIELD_CACHE);
	}

	/**
	 * 刷新集群信息，当集群信息发生变更时调用
	 * 
	 */
	public void refreshCluster() {
		connectionHandler.renewSlotCache();
	}

	/**
	 * 同步读取所有数据. 与syncAndReturnAll()相比，sync()只是没有对数据做反序列化
	 */
	public void sync() {
		innerSync(null);
	}

	/**
	 * 同步读取所有数据 并按命令顺序返回一个列表
	 * 
	 * @return 按照命令的顺序返回所有的数据
	 */
	public List<Object> syncAndReturnAll() {
		List<Object> responseList = new ArrayList<Object>();

		innerSync(responseList);

		return responseList;
	}
	
	/**
	 * 
	 * @param pattern 匹配模式
	 * @param jedisCluster JedisCluser对象
	 * @return 模糊匹配的所有Set值
	 */
	public Set<String> keys(String pattern, JedisCluster jedisCluster) {
		Set<String> keys = new TreeSet<String>();
		
		Map<String, JedisPool> clusterNodes = jedisCluster.getClusterNodes();
		for(String k : clusterNodes.keySet()) {
			JedisPool jedisPool = clusterNodes.get(k);
			Jedis jedis = jedisPool.getResource();
			try {
				keys.addAll(jedis.keys(pattern));
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			} finally {
				jedis.close();
			}
		}
		return keys;
	}

	/**
	 * 
	 * @param formatted formatted
	 */
	private void innerSync(List<Object> formatted) {
		HashSet<Client> clientSet = new HashSet<Client>();
		try {
			for (Client client : clients) {
				/**
				 *  在sync()调用时其实是不需要解析结果数据的，但是如果不调用get方法，发生了JedisMovedDataException这样的错误应用是不知道的，因此需要调用get()来触发错误。
				 *  其实如果Response的data属性可以直接获取，可以省掉解析数据的时间，然而它并没有提供对应方法，要获取data属性就得用反射，不想再反射了，所以就这样了
				 */
				
				Object data = generateResponse(client.getOne()).get();
				if (null != formatted) {
					formatted.add(data);
				}

				/**
				 *  size相同说明所有的client都已经添加，就不用再调用add方法了
				 */
				if (clientSet.size() != jedisMap.size()) {
					clientSet.add(client);
				}
			}
			
		} catch (JedisRedirectionException jre) {
			if (jre instanceof JedisMovedDataException) {
				/**
				 *  if MOVED redirection occurred, rebuilds cluster's slot cache,
				 *  recommended by Redis cluster specification
				 */
				
				refreshCluster();
			}

		} finally {
			if (clientSet.size() != jedisMap.size()) {
				/**
				 *  所有还没有执行过的client要保证执行(flush)，防止放回连接池后后面的命令被污染
				 */
				for (Jedis jedis : jedisMap.values()) {
					if (clientSet.contains(jedis.getClient())) {
						continue;
					}

					flushCachedData(jedis);
				}
			}

			hasDataInBuf = false;
			close();
		}
	}

	public void close() {
		clean();
		clients.clear();
		for (Jedis jedis : jedisMap.values()) {
			if (hasDataInBuf) {
				flushCachedData(jedis);
			}
			jedis.close();
		}

		jedisMap.clear();

		hasDataInBuf = false;
	}

	/**
	 * 
	 * @param jedis jedis对象
	 */
	private void flushCachedData(Jedis jedis) {
		try {
			jedis.getClient().getAll();
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Override
	protected Client getClient(String key) {
		byte[] bKey = SafeEncoder.encode(key);

		return getClient(bKey);
	}

	@Override
	protected Client getClient(byte[] key) {
		Jedis jedis = getJedis(JedisClusterCRC16.getSlot(key));

		Client client = jedis.getClient();
		clients.add(client);

		return client;
	}

	/**
	 * 
	 * @param slot 节点
	 *            
	 * @return jedis对象
	 */
	private Jedis getJedis(int slot) {
		JedisPool pool = clusterInfoCache.getSlotPool(slot);

		/**
		 *  根据pool从缓存中获取Jedis
		 */
		Jedis jedis = jedisMap.get(pool);
		if (null == jedis) {
			jedis = pool.getResource();
			jedisMap.put(pool, jedis);
		}

		hasDataInBuf = true;
		return jedis;
	}

	/**
	 * 
	 * @param cls 类
	 * @param fieldName 字段的名称
	 * @return 返回该域值
	 */
	private static Field getField(Class<?> cls, String fieldName) {
		try {
			Field field = cls.getDeclaredField(fieldName);
			field.setAccessible(true);

			return field;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * 
	 * @param <T> 抽象类型
	 * @param obj 类的对象
	 * @param field 字段名称
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static <T> T getValue(Object obj, Field field) {
		try {
			return (T) field.get(obj);
		} catch (Exception e) {
			logger.error("get value fail : " + e.getMessage(), e);
			return null;
		}
	}
	
}
