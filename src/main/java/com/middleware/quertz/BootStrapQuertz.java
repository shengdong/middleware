package com.middleware.quertz;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.Scheduler;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class BootStrapQuertz {

	public static void main(String[] args) throws Exception {
		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
		scheduler.scheduleJob(JobBuilder.newJob(QuertzJob.class).withIdentity("test1Job").build(), 
				TriggerBuilder.newTrigger()
				.withIdentity("test2Trigger").
				withSchedule(CronScheduleBuilder.cronSchedule("0 17-30/1 11 * * ? *")).startNow().build());
		scheduler.start();
		
//		JobDetail detail = JobBuilder.newJob(QuertzJob.class).withIdentity("job_1", "group1").build();
//		
//		@SuppressWarnings("static-access")
//		SimpleScheduleBuilder builder = SimpleScheduleBuilder.simpleSchedule().repeatMinutelyForTotalCount(100);
//		
//		Trigger trigger = TriggerBuilder.newTrigger().withIdentity("trigger_1", "tGroup1").startNow().withSchedule(builder).build();
//		
//		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
//		
//		scheduler.start();
//		
//		scheduler.scheduleJob(detail, trigger);
		
	}

}
