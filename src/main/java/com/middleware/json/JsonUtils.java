package com.middleware.json;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonUtils {
    public static void main(String[] args) {
        List<Map<String, String>> list = new ArrayList<Map<String, String>>(2);
        Map<String, String> map = new HashMap<String, String>(4);
        map.put("name", "s.he");
        map.put("address", "hangzhou");
        map.put("age", "25");
        map.put("phone", "7551");

        list.add(map);

        Map<String, String> map1 = new HashMap<String, String>(4);
        map1.put("name", "wangmengjie");
        map1.put("address", "hangzhou");
        map1.put("age", "20");
        map1.put("phone", "182");
        list.add(map1);
        Gson gson = new Gson();
        System.out.println(gson.toJson(list));
    }
}
