package com.middleware.arithmetic;

/**
 * create by s.he
 * on 2017/12/5 0005
 */
public class OperationClass {
    public static byte getXor(byte[] datas) {
        byte result = datas[0];

        for(int i=1; i<datas.length; i++) {
            result ^= datas[i];
        }

        return result;
    }

    public static void main(String[] args) {
        byte[] data = new byte[]{1, 2};
        System.out.println(getXor(data));
    }
}
