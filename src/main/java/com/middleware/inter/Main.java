package com.middleware.inter;

/**
 * create by s.he
 * on 2017/11/27 0027
 */
public class Main {
    public static void main(String[] args) {
        IRunner runner = new HelloRunner();
        runner.run();
    }
}
