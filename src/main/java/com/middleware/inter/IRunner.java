package com.middleware.inter;

/**
 * create by s.he
 * on 2017/11/27 0027
 */
public interface IRunner {
    void run();
}
