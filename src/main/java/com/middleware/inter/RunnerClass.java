package com.middleware.inter;

/**
 * create by s.he
 * on 2017/11/27 0027
 */
public abstract class RunnerClass implements IRunner{

    @Override
    public void run() {
        execute();
    }

    public abstract void execute();
}
