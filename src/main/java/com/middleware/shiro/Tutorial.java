package com.middleware.shiro;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;

/**
 * Created by s.he
 * on 2017\8\14 0014.
 */
public class Tutorial {
    private static  final Logger logger = Logger.getLogger(Tutorial.class);

    public static void main(String[] args) {
        logger.info("My First Apache shiro Application");

        Factory<SecurityManager> factory = new IniSecurityManagerFactory("");

        SecurityUtils.setSecurityManager(factory.getInstance());

        Subject subject = SecurityUtils.getSubject();

        Session session = subject.getSession();
        session.setAttribute("someKey", "aValue");

        String value = (String) session.getAttribute("someKey");
        if(StringUtils.equals(value, "aValue")) {
            System.out.println("Tutorial.main" + value);
        }

        if(!subject.isAuthenticated()) {
            UsernamePasswordToken token = new UsernamePasswordToken("lonestarr", "s.he");
            token.setRememberMe(true);

            try {
                subject.login(token);
            } catch (UnknownAccountException uae) {
                logger.info("There is no user with username of " + token.getPrincipal());
            } catch (IncorrectCredentialsException ice) {
                logger.info("Password for account " + token.getPrincipal() + " was incorrect!");
            } catch (LockedAccountException lae) {
                logger.info("The account for username " + token.getPrincipal() + " is locked.  " +
                        "Please contact your administrator to unlock it.");
            }
            // ... catch more exceptions here (maybe custom ones specific to your application?
            catch (AuthenticationException ae) {
                //unexpected condition?  error?
            }
        }

        if (subject.hasRole("schwartz")) {
            logger.info("May the Schwartz be with you!");
        } else {
            logger.info("Hello, mere mortal.");
        }

        //test a typed permission (not instance-level)
        if (subject.isPermitted("lightsaber:weild")) {
            logger.info("You may use a lightsaber ring.  Use it wisely.");
        } else {
            logger.info("Sorry, lightsaber rings are for schwartz masters only.");
        }

        //a (very powerful) Instance Level permission:
        if (subject.isPermitted("winnebago:drive:eagle5")) {
            logger.info("You are permitted to 'drive' the winnebago with license plate (id) 'eagle5'.  " +
                    "Here are the keys - have fun!");
        } else {
            logger.info("Sorry, you aren't allowed to drive the 'eagle5' winnebago!");
        }

        //all done - log out!
        subject.logout();

    }
}
