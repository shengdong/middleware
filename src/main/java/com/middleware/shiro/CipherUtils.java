/*
 * Copyright (c) 2016.  Created by ZZP
 */

package com.middleware.shiro;

import org.apache.shiro.codec.Hex;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;

/**
 * Created by zzp on 4/2/16.
 */
public class CipherUtils {

    private static final String KEY = "ngk5PDLRnTFUzH5XEENopl9UOOc1XdS4";

    private static final String AES = "AES";

    private static final String ALGORITHM = "SHA1PRNG";

    public static String encrypt(String content) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance(AES);
        SecureRandom random = SecureRandom.getInstance(ALGORITHM);
        random.setSeed(KEY.getBytes());
        kgen.init(128, random);
        SecretKey secretKey = kgen.generateKey();
        SecretKeySpec key = new SecretKeySpec(secretKey.getEncoded(), AES);
        Cipher cipher = Cipher.getInstance(AES);// 创建密码器
        byte[] byteContent = content.getBytes("utf-8");
        cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化
        byte[] result = cipher.doFinal(byteContent);
        return Hex.encodeToString(result);
    }

    public static String decrypt(String content) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance(AES);
        SecureRandom random = SecureRandom.getInstance(ALGORITHM);
        random.setSeed(KEY.getBytes());
        kgen.init(128, random);
        SecretKey secretKey = kgen.generateKey();
        SecretKeySpec key = new SecretKeySpec(secretKey.getEncoded(), AES);
        Cipher cipher = Cipher.getInstance(AES);// 创建密码器
        cipher.init(Cipher.DECRYPT_MODE, key);// 初始化
        byte[] result = cipher.doFinal(Hex.decode(content));//解密
        return new String(result);
    }
}
