package com.middleware.shiro;

import org.apache.commons.codec.binary.Hex;
import org.apache.shiro.codec.Base64;
import org.junit.*;
import org.junit.Test;

/**
 * Created by Idea
 * date: 2017/7/28 0028.
 * time: 14:00.
 * user: s.he
 */

public class EncodeTest {


    @org.junit.Test
    public void testBase64() {
        String str1 = "hellodfasdhfjafakdfjskdfjaksdjfksajfkafsfasf";
        String baseEncode64 = Base64.encodeToString(str1.getBytes());
        System.out.println("EncodeTest.test: " + baseEncode64);
        String str2 = Base64.decodeToString(baseEncode64.getBytes());

        Assert.assertEquals(str1, str2);
    }

    @Test
    public void testHex() throws  Exception{
        String str = "hello";
        String hexEncode = Hex.encodeHexString(str.getBytes());
        System.out.println("EncodeTest.testHex:" + hexEncode);

        String str2 = new String(Hex.decodeHex(hexEncode.toCharArray()));
        Assert.assertEquals(str, str2);
    }

}
