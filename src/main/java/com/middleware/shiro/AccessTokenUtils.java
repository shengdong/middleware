/*
 * Copyright (c) 2016.  Created by ZZP
 */

package com.middleware.shiro;

import java.util.Date;

/**
 * Token工具
 * Created by zzp on 4/5/16.
 */
public class AccessTokenUtils {

    /**
     * 生成新的token
     *
     * @param username 用户名
     * @return token序列串
     * @throws Exception
     */
    public static String makeToken(String username) throws Exception {
        return CipherUtils.encrypt(username + ":" + new Date().getTime());
    }

    /**
     * 解析token
     *
     * @param token token序列串
     * @return 用户号码和时间戳数组
     * @throws Exception
     */
    public static String[] parseToken(String token) throws Exception {
        String decrypted = CipherUtils.decrypt(token);
        if (decrypted.indexOf(":") < 0)
            throw new IllegalArgumentException("Illegal Token");
        return decrypted.split(":");
    }

    /**
     * 解析token并返回用户Id
     *
     * @param token
     * @return
     * @throws Exception
     */
    public static String getUsernameFromToken(String token) throws Exception {
        return parseToken(token)[0];
    }
}
