package com.middleware.shiro;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
/**
 * Created by Idea
 * date: 2017/7/28 0028.
 * time: 11:26.
 * user: s.he
 */
public class Test {
    private static final Logger logger = Logger.getLogger(Test.class);

    public static void main(String[] args) {
        Factory<SecurityManager> factory =
                new IniSecurityManagerFactory("classpath:shiro.ini");
        SecurityManager manager = factory.getInstance();

        SecurityUtils.setSecurityManager(manager);

        Subject subject =  SecurityUtils.getSubject();
        Session session = subject.getSession();
        session.setAttribute("someKey", "aValue");

        String value = (String) session.getAttribute("someKey");

        if(StringUtils.equals(value, "aValue")) {
            logger.info("Retrieved the correct value! [" + value + "]");
        }

        if (!subject.isAuthenticated()) {
            UsernamePasswordToken token = new UsernamePasswordToken("root", "secret");

            token.setRememberMe(true);
            try {
                subject.login(token);
                logger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<User [" + subject.getPrincipal() + "] logged in successfully.");
            } catch (UnknownAccountException uae) {
                logger.error("There is no user with username of " + token.getPrincipal());
            } catch (IncorrectCredentialsException ice) {
                logger.error("Password for account " + token.getPrincipal() + " was incorrect!");
            } catch (LockedAccountException lae) {
                logger.error("The account for username " + token.getPrincipal() + " is locked.  " +
                        "Please contact your administrator to unlock it.");
            }
            // ... catch more exceptions here (maybe custom ones specific to your application?
            catch (AuthenticationException ae) {
                //unexpected condition?  error?
            }
        }

        if (subject.hasRole("admin")) {
            logger.info("May the Schwartz be with you!");
        } else {
            logger.info("Hello, mere mortal.");
        }

        //test a typed permission (not instance-level)
        if (subject.isPermitted("lightsaber:weild")) {
            logger.info("You may use a lightsaber ring.  Use it wisely.");
        } else {
            logger.info("Sorry, lightsaber rings are for schwartz masters only.");
        }

        //a (very powerful) Instance Level permission:
        if (subject.isPermitted("winnebago:drive:eagle5")) {
            logger.info("You are permitted to 'drive' the winnebago with license plate (id) 'eagle5'.  " +
                    "Here are the keys - have fun!");
        } else {
            logger.info("Sorry, you aren't allowed to drive the 'eagle5' winnebago!");
        }

    }
}
