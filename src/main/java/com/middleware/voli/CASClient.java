package com.middleware.voli;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


public class CASClient {
	private AtomicInteger atomicInteger = new AtomicInteger(0);
	
	private int i = 0;
	
	
	public static void main(String[] args) {
		final CASClient client = new CASClient();
		List<Thread> lists = new ArrayList<Thread>(600);
		long start = System.currentTimeMillis();
		for (int i = 0; i < 100; i++) {
			Thread t = new Thread(new Runnable() {
				
				@Override
				public void run() {
					for(int i=0; i<10000; i++) {
						client.count();
						client.safeCount();
					}
				}
			});
			lists.add(t);	
			
		}
	}
	
	private void safeCount() {
		for(;;) {
			int i = atomicInteger.get();
			boolean suc = atomicInteger.compareAndSet(i, ++i);
			if(suc) {
				break;
			}
		}
	}
	
	private void count() {
		i++;
	}

}
