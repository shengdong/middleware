package com.middleware.mongoDB;

import com.mongodb.BasicDBObject;
import com.mongodb.Cursor;
import com.mongodb.MongoClient;
import com.mongodb.QueryOperators;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.DeleteResult;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Created by Idea
 * date: 2017/7/20 0020.
 * time: 9:56.
 * user: s.he
 */
public class MainMongoDBTest {
    private static final Logger logger = Logger.getLogger(MainMongoDBTest.class);

    private static final String HOST = "localhost";

    private static final int PORT = 27017;

    private static final String DATA_BASE_NAME = "dongDB";

    private MongoClient mongoClient;

    private MongoDatabase mongoDatabase;

    private MongoCollection<Document> mongoCollection;


    @Before
    public void setUp() throws Exception {
        mongoClient = new MongoClient(HOST, PORT);
        mongoDatabase = mongoClient.getDatabase(DATA_BASE_NAME);
        mongoCollection =  mongoDatabase.getCollection("dongDB");
    }


    @Test
    public void testDeleteCollection() {
        mongoDatabase.getCollection("test").drop();
    }

    @Test
    public void testCreateCollection() {
        mongoDatabase.createCollection("test");
    }

    @Test
    public void testFindDocument() {
        FindIterable<Document>  findIterable = mongoCollection.find();

        for(Document document : findIterable) {
            Set<String> sets = document.keySet();
            for(String key : sets) {
                logger.info("key : " + key + ", value:" + document.get(key));
            }
        }
    }

    @Test
    public void testUpdateDocument() {
//        mongoCollection.updateMany(Filters.eq("likes", 200), new Document("$set", new Document("likes", 100)));
          Document query = new Document();
          query.append("_id", "597019c3eecbc611ec6dea5e");

          Document updateVal = new Document();
          updateVal.append("username", "何胜东1");
          updateVal.append("age", 36);
          updateVal.append("address", "杭州下城区1");
          updateVal.append("likes", 400);

          Document update = new Document();
          update.append("$set", updateVal);

         mongoCollection.updateOne(query, update, new UpdateOptions().upsert(true));
    }

    @Test
    public void testInsertDocument() {
        List<Document> list = new ArrayList<Document>(10);
        Document document = new Document();
        document.append("likes", 100);
        document.append("username", "w.mj");
        document.append("age", 20);
        document.append("address", "杭州上城区");
        list.add(document);
        mongoCollection.insertMany(list);
    }

    @Test
    public void testDeleteDocument() {
        DeleteResult deleteResult = mongoCollection.deleteOne(Filters.eq("likes", 100));
        long t = deleteResult.getDeletedCount();
        logger.info("t:" + t);
    }

    @Test
    public void testQueryMultiCondition() {
        BasicDBObject ageObj = new BasicDBObject("age", new BasicDBObject("$gt", 19));
        BasicDBObject nameObj = new BasicDBObject("username", "s.he");

        BasicDBObject sexObj = new BasicDBObject("name","菜鸟教程");
        BasicDBObject andObj = new BasicDBObject("$and", Arrays.asList(ageObj, nameObj));
        BasicDBObject obj = new BasicDBObject("$or", Arrays.asList(sexObj, andObj));

        FindIterable<Document> findIterable = mongoCollection.find(obj);
        for(Document document : findIterable) {
            logger.info(document.get("_id"));
        }
    }

    @Test
    public void testQuery() {
        BasicDBObject ageObj = new BasicDBObject("age", new BasicDBObject("$gt", 19));
        BasicDBObject nameObj = new BasicDBObject("username", "s.he");
        BasicDBObject sexObj = new BasicDBObject("name","菜鸟教程");
        Query query = new Query();
        BasicDBObject object = query.and(ageObj).and(nameObj).or(sexObj).getResult();
        FindIterable<Document> findIterable = mongoCollection.find(object);
        for(Document document : findIterable) {
            logger.info(document.get("_id"));
        }
    }

    @Test
    public void testQueryOperators() {

    }

}

class Query {
    private BasicDBObject result = new BasicDBObject();


    public  Query and(BasicDBObject basicDBObject) {
        result = new BasicDBObject(QueryOperators.AND, Arrays.asList(result, basicDBObject));
        return this;
    }

    public  Query or(BasicDBObject basicDBObject) {
        result = new BasicDBObject(QueryOperators.OR, Arrays.asList(result, basicDBObject));
        return this;
    }

    public BasicDBObject getResult() {
        return result;
    }
}

