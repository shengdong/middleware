package com.middleware.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

public class GsonList {

	public static void main(String[] args) {
		List<User> lists = new ArrayList<User>(10);
		lists.add(new User("hsd", "123456", 1));
		lists.add(new User("hsd1", "1234567", 2));
		lists.add(new User("hsd2", "123458", 3));
		lists.add(new User("hsd3", "123459", 4));
		Gson gson = new Gson();
		Map<String, List<User>> map = new HashMap<String, List<User>>();
		map.put("user", lists);
		System.out.println(gson.toJson(map));
	}
}
