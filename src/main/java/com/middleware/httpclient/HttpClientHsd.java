package com.middleware.httpclient;

import com.google.gson.Gson;
import com.middleware.md5.MD5Utils;
import com.mongodb.util.JSON;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpClientHsd {
	private static final Logger logger = Logger.getLogger(HttpClientHsd.class);
	
	public void httpGet(String uri) throws Exception, IOException {
		String result = "";
		HttpGet get = new HttpGet(uri);
		HttpClient client = HttpClients.createDefault();
		HttpResponse response = client.execute(get);
		
		if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			result = EntityUtils.toString(response.getEntity(),"utf-8");
		}
		logger.info(result);
	}

	public String httpPost(Header header, String url, HttpEntity entity) throws Exception {
		String result = "";
		HttpClient client = HttpClients.createDefault();
		HttpPost post = new HttpPost(url);
//		post.addHeader("Content-type","application/json; charset=utf-8");
		post.setHeader(header);
		post.setEntity(entity);
		HttpResponse response =  client.execute(post);
		if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			result = EntityUtils.toString(response.getEntity(),"utf-8");
		}
		return result;
	}

	@Test
	public void testLoginJson() throws Exception{
		String url = "http://console-api-test.qibeitech.com/console/login";
		String username = "admin";
		String password = "admin";
		Map<String, String> mp = new HashMap<String, String>(2);
		mp.put("username", username);
		mp.put("password", password);

		Gson gson = new Gson();
		String loginDto = gson.toJson(mp);
		logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + loginDto);

		HttpEntity entity = new StringEntity(loginDto, Charset.forName("UTF-8"));
		Header header = new BasicHeader("Accept", "application/json");
		String result = httpPost(null, url, entity);
		Map<String, Object> map = (Map<String, Object>) JSON.parse(result);
	}

	@Test
	public void testAccept() throws Exception {
		String url = "";
		/**
		 * 设置请求头
		 */
		Header header = new BasicHeader("token", "");

		/**
		 * 设置参数
		 */
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("", ""));

		HttpEntity entity = new UrlEncodedFormEntity(nvps);
		String result = httpPost(header, url, entity);
		logger.info(result);



	}

	@Test
	public void test1() throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		map.put("mobile", "18815287551");


		String url = "http://192.168.4.40:28086/user/query/mobile";

		if(map.size() != 0) {
			StringBuffer buffer = new StringBuffer("?");
			for(String key : map.keySet()) {
				buffer.append(key + "=" + map.get(key) + "&");
			}
			url = url + buffer.toString().substring(0, buffer.toString().length() - 1);
		}
		HttpGet get = new HttpGet(url);
		HttpClient client = HttpClients.createDefault();
		Header header = new BasicHeader("AK", "a3175a452c7a8fea80c62a198a40f6c9");
		get.setHeader(header);
		HttpResponse response = client.execute(get);
		if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			String result = EntityUtils.toString(response.getEntity(),"utf-8");
			logger.info(result);
		}

	}


	public void login() {
		String url = "http://127.0.0.1:8080/console/operator/login";
		String username = "13588755652";
		String password = "123456";
		String newPassword = MD5Utils.encodeMsg(MD5Utils.encodeMsg(password));


	}
}
