package com.middleware.hashmap;

import org.apache.commons.collections.map.HashedMap;

import java.util.Map;

/**
 * create by s.he
 * on 2017/11/25 0025
 */
public class Main {
    public static void main(String[] args) {
        Map<String, String> map = new HashedMap();
       String hello = map.put("2", "hello");
        System.out.println(map.get("2"));
    }
}
