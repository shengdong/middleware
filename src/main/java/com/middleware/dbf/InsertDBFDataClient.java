
package com.middleware.dbf;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.middleware.db.MySqlDB;

public class InsertDBFDataClient {
	private static final Logger logger = Logger.getLogger(InsertDBFDataClient.class);

	private MySqlDB db;

	public InsertDBFDataClient() {
		db = new MySqlDB();
	}

	public boolean insertDB(List<String> list, String insertSql) {
		Connection connection = db.getConnection();

		PreparedStatement statement = null;
		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(insertSql);
			int count = 0;
			for (String data : list) {
				count++;
				String datas[] = data.split(";;");
				for (int i = 0; i < datas.length; i++) {
					statement.setString(i+1, datas[i] == null? "" : datas[i].trim());
				}
				statement.addBatch();
				if (count % 1000 == 0) {
					statement.executeBatch();
					connection.commit();
				}
			}

			statement.executeBatch();
			connection.commit();
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			try {
				connection.rollback();
			} catch (SQLException e1) {
				logger.error(e1.getMessage(), e1);
			}
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			try {
				if (connection != null) {
					connection.close();
				}

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}

		return false;
	}

}
