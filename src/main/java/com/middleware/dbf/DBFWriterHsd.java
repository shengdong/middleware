package com.middleware.dbf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.linuxense.javadbf.DBFField;
import com.linuxense.javadbf.DBFWriter;
import com.middleware.db.MySqlDB;
import com.middleware.util.ConfigUtil;

public class DBFWriterHsd {
	private static final Logger logger = Logger.getLogger(DBFWriterHsd.class);
	
	private MySqlDB sqlDB;
	
	public DBFWriterHsd() {
		sqlDB = new MySqlDB();
	}
	
	public static void main(String[] args) {
		String sql = "select * from HK";
		String todayString = ConfigUtil.getToday("YYYYMMdd");
		String path = "/home/hsd/data/result" + File.separator + "HK" + todayString + ".dbf";
		DBFWriterHsd writerHsd = new DBFWriterHsd();
		writerHsd.writeDBF(sql, path);
	}
	
	public boolean writeDBF(String sql, String path) {
		String split = ";";
		String header = "TDATE;TCLOSE;SYMOBL;FSZSH;FTYPE";
		String[] headerData = header.split(split);
		OutputStream stream = null;
		int length = header.split(split).length;
		try {
			DBFWriter writer = new DBFWriter();
			List<String> results = getResult(sql, header, split);
			DBFField[] fields = new DBFField[length];
			for(int i=0; i<length; i++) {
				DBFField field = new DBFField();
				field.setName(headerData[i]);
				field.setDataType(DBFField.FIELD_TYPE_C);
				field.setFieldLength(headerData[i].length());
				fields[i] = field;
			}
			writer.setFields(fields);
			
			Object[] rowData = new Object[length];  
			for(String data : results) {
				rowData = data.split(split);
				writer.addRecord(rowData);
			}
			stream = new FileOutputStream(path);
			writer.write(stream);
			
			
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		} finally {
			try {
				if(stream != null) {
					stream.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
	
	private List<String> getResult(String sql, String header, String split) {
		int length = header.split(split).length;
		List<String> lists = new ArrayList<String>();
		Connection connection = sqlDB.getConnection();
		PreparedStatement statement = null;
		ResultSet set = null;
		try {
			connection = sqlDB.getConnection();
			statement = connection.prepareStatement(sql);
			set = statement.executeQuery();
			StringBuffer buffer = new StringBuffer();
			while(set.next()) {
				
				for(int i=1; i<=length; i++) {
					String data = set.getString(i);
					if(i == length) {
						buffer.append(data);
					} else {
						buffer.append(data + split);
					}
				}
				lists.add(buffer.toString());
				buffer.delete(0, buffer.length());
			}
			return lists;
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		} finally {
			try {
				if(set != null) {
					set.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			
			try {
				if(statement != null) {
					statement.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			
			try {
				if(connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
}
