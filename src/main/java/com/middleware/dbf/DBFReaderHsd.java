package com.middleware.dbf;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.linuxense.javadbf.DBFField;
import com.linuxense.javadbf.DBFReader;

public class DBFReaderHsd {
	private static final Logger logger = Logger.getLogger(DBFReaderHsd.class);
	
	public List<String> read(String path) {
		List<String> result = new ArrayList<String>();
		InputStream stream = null;
		try {
			stream = new FileInputStream(path);
			DBFReader reader = new DBFReader(stream);
			
			int fieldsCount = reader.getFieldCount();
			for(int i=0; i < fieldsCount; i++) {
				DBFField dbfField = reader.getField(i);
				logger.info(dbfField.getName());
			}
			
			Object[] rowValues = null;
			StringBuffer buffer = new StringBuffer();
			while((rowValues = reader.nextRecord()) != null) {
				for(int i=0; i<rowValues.length; i++) {
					if(i == rowValues.length - 1) {
						buffer.append(rowValues[i]);
					} else {
						buffer.append(rowValues[i] + ";;");
					}
					
				}
				result.add(buffer.toString());
				buffer.delete(0, buffer.length());
			}
			return result;
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		} finally {
			try {
				if(stream != null) {
					stream.close();
				}
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
	
	public void write(List<String> data, String path) {
	}
	
	public static void main(String[] args) {
//		DBFHandler handler = new DBFHandler();
//		List<String> result = handler.read("/home/hsd/data/gz/HK20170313.dbf");
//		String insertSql = "insert into HK(TDATE, TCLOSE, SYMBOL, FSZSH, FTYPE) values(?,?,?,?,?)";
//		InsertDBFDataClient data = new InsertDBFDataClient();
//		boolean flag = data.insertDB(result, insertSql);
//		logger.info(flag);
	}
}
