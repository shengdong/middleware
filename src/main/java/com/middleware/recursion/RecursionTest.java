package com.middleware.recursion;

import org.apache.commons.collections.map.HashedMap;

import java.util.Map;

/**
 * create by s.he
 * on 2017/11/28 0028
 */
public class RecursionTest {
    public static Map<Integer, Integer> map = new HashedMap();
    static {
        map.put(1, 0);
        map.put(2, 1);
        map.put(3, 2);
        map.put(4, 3);
        map.put(5, 4);
        map.put(6, 5);
        map.put(7, 0);
        map.put(8, 7);
        map.put(9, 7);
        map.put(10, 9);
        map.put(11, 8);
        map.put(12, 11);
        map.put(13, 12);
        map.put(14, 11);
    }

    public boolean check(Integer key, Integer value) {
        Integer parent = map.get(value);
        if(parent == 0) {
            System.out.println("找到根节点，没有发现闭环，可以更新");
            return true;
        }

        if(parent == key) {
            System.out.println("发现闭环，不允许更新");
            return false;
        }
        return check(key, parent);
    }

    public static void main(String[] args) {
        RecursionTest recursionTest = new RecursionTest();
        boolean flag = recursionTest.check(12, 13);
        System.out.println(flag);
    }
}
