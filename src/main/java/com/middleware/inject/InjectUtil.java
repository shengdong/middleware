package com.middleware.inject;

import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class InjectUtil {

  	public static void injectValue(String fieldName, Object value, Object obj) throws Exception {
		Class<?> clazz = obj.getClass();
		Field[] fields = clazz.getDeclaredFields();
		for(Field field : fields) {
			field.setAccessible(true);
			if(StringUtils.equals(field.getName(), fieldName)) {
				// for final field
				Field modifiersField = Field.class.getDeclaredField("modifiers");  
				modifiersField.setAccessible(true);  
				modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL); 
				
				field.set(obj, value);
			}
		}
	}

}


