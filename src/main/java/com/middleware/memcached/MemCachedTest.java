package com.middleware.memcached;

import com.whalin.MemCached.MemCachedClient;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.net.InetSocketAddress;

/**
 * Created by Idea
 * date: 2017/7/20 0020.
 * time: 14:19.
 * user: s.he
 */
public class MemCachedTest {
    private static final Logger logger = Logger.getLogger(MemCachedTest.class);

    private MemCachedClient client = null;

    private static final String HOST = "127.0.0.1";


    @Before
    public void setUp() throws Exception{
        client = new MemCachedClient(HOST);

        System.out.println("MemCachedTest.setUp" + client);

    }

    @Test
    public  void testSet() {
        boolean flag = client.set("username", 3);
        logger.info(flag);
    }
}
