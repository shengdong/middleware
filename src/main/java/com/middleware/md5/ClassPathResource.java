package com.middleware.md5;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClassPathResource {
    private static final List<String> mobiles = Arrays.asList(
            "134", "135", "136", "137", "138", "139", "150", "151", "157", "158" , "159", "187", "188",
            "130", "131", "132", "152", "155", "156", "185", "186",
            "133", "153", "180", "189"
    );

    public static boolean isMobileNO(String mobiles) {
//        Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
//        Matcher m = p.matcher(mobiles);
//        System.out.println(m.matches() + "---");
//        return m.matches();
        if(mobiles.length() != 11) {
            return false;
        }

        String mobileHeader = mobiles.substring(0, 3);
        System.out.println(mobileHeader);
        if(!mobiles.contains(mobileHeader)) {
            return false;
        }

        String mobileFooter = mobiles.substring(3, 11);
        System.out.println(mobileFooter);
        String regEx = "^\\d+$";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(mobileFooter);
        return m.matches();
    }




    public static void main(String[] args) throws IOException {

        System.out.println(ClassPathResource.isMobileNO("1881528755z"));
    }

}
