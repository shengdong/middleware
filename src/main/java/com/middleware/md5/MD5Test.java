package com.middleware.md5;

import java.security.MessageDigest;

/**
 * create by s.he
 * on 2017/12/4 0004
 */
public class MD5Test {
    public static void main(String[] args) throws Exception{
        String s = "admin";
        System.out.println(new String(encryptMD5(s.getBytes())));
    }

    public static byte[] encryptMD5(byte[] data) throws Exception{
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(data);

        return messageDigest.digest();
    }
}
