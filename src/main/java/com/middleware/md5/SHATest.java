package com.middleware.md5;

import java.security.MessageDigest;

/**
 * create by s.he
 * on 2017/12/4 0004
 */
public class SHATest {
    public static void main(String[] args) throws Exception{
        String s = "admin";
        System.out.println(new String(encryptSHA(s.getBytes())));
    }

    public static byte[] encryptSHA(byte[] data) throws Exception {

        MessageDigest sha = MessageDigest.getInstance("SHA");
        sha.update(data);

        return sha.digest();

    }
}
