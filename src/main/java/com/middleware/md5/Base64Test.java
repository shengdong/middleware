package com.middleware.md5;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * create by s.he
 * on 2017/12/4 0004
 */
public class Base64Test {
    public static byte[] decryptBase64(String key) throws Exception{
        return (new BASE64Decoder()).decodeBuffer(key);
    }

    public static String encryptBASE64(byte[] key) throws Exception{
        return (new BASE64Encoder()).encode(key);
    }

    public static void main(String[] args) throws Exception{
        String s = "admin";
        //加密之后
        System.out.println(encryptBASE64(s.getBytes()));

        //解密之后
        System.out.println(new String(decryptBase64(encryptBASE64(s.getBytes()))));
    }
}
