package com.middleware.md5;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

public class MD5Utils {
	private static final Logger logger = Logger.getLogger(MD5Utils.class);

	public static String encodeMsg(String message) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(message.getBytes());
			byte b[] = md.digest();

			int i;

			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			return buf.toString();
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}
	
	public static void main(String[] args) {
		System.out.println(getAuthCode(4));
	}

	private static  String getAuthCode(int len) {
		StringBuilder result = new StringBuilder("");
		for(int i=0; i<len; i++) {
			result.append(new Double(Math.random() * 10).intValue());
		}

		return result.toString();
	}
}
