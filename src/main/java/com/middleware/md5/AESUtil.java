package com.middleware.md5;

import org.springframework.scheduling.annotation.EnableScheduling;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * create by s.he
 * on 2017/12/4 0004
 */
public class AESUtil {
    private static final String ENCRYPT_KEY = "AES";

    public static byte[] encrypt(String content, String password) {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance(ENCRYPT_KEY);
            keyGenerator.init(128, new SecureRandom(password.getBytes()));
            SecretKey secretKey = keyGenerator.generateKey();

            byte[] encoded = secretKey.getEncoded();
            SecretKeySpec key = new SecretKeySpec(encoded, ENCRYPT_KEY);
            Cipher cipher = Cipher.getInstance(ENCRYPT_KEY);

            byte[] contentBytes = content.getBytes("utf-8");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] result = cipher.doFinal(contentBytes);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] decrypt(byte[] content, String password) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance(ENCRYPT_KEY);
            kgen.init(128, new SecureRandom(password.getBytes()));
            SecretKey secretKey = kgen.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, ENCRYPT_KEY);
            Cipher cipher = Cipher.getInstance(ENCRYPT_KEY);// 创建密码器
            cipher.init(Cipher.DECRYPT_MODE, key);// 初始化
            byte[] result = cipher.doFinal(content);
            return result; // 加密
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) {
        String username = "admin";
        String password = "123456";

       byte[] data = encrypt(username, password);
        System.out.println(new String(decrypt(data, password)));
    }
}
