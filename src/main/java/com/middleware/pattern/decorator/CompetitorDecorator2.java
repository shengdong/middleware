package com.middleware.pattern.decorator;

/**
 * Created by Idea
 * date: 2017/7/26 0026.
 * time: 13:47.
 * user: s.he
 */
public class CompetitorDecorator2 extends CompetitorDecorator{
    public CompetitorDecorator2(Competitor competitor) {
        super(competitor);
    }

    @Override
    public void sing() {
        this.playMusic();
        super.sing();
    }

    private void playMusic() {
        System.out.println("播放背景音乐.........");
    }
}
