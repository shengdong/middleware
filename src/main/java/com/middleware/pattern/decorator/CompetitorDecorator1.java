package com.middleware.pattern.decorator;

/**
 * Created by Idea
 * date: 2017/7/26 0026.
 * time: 13:46.
 * user: s.he
 */
public class CompetitorDecorator1 extends CompetitorDecorator {

    public CompetitorDecorator1(Competitor competitor) {
        super(competitor);
    }

    @Override
    public void sing() {
        dance();
        super.sing();

    }

    private void dance() {
        System.out.println("翩翩起舞");
    }
}
