package com.middleware.pattern.decorator;

/**
 * Created by Idea
 * date: 2017/7/26 0026.
 * time: 13:51.
 * user: s.he
 */
public class Client {
    public static void main(String[] args) {
        Competitor competitor = new Competitor();
        CompetitorDecorator decorator = new CompetitorDecorator(competitor);
        decorator = new CompetitorDecorator3(new CompetitorDecorator2(new CompetitorDecorator1(decorator)));

        decorator.sing();
    }
}
