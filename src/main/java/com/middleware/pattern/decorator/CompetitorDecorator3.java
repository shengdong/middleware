package com.middleware.pattern.decorator;

/**
 * Created by Idea
 * date: 2017/7/26 0026.
 * time: 13:48.
 * user: s.he
 */
public class CompetitorDecorator3 extends CompetitorDecorator {

    public CompetitorDecorator3(Competitor competitor) {
        super(competitor);
    }

    @Override
    public void sing() {
        this.introduceBackground();
        super.sing();
    }

    private void introduceBackground() {
        System.out.println("悲惨背景介绍，博取同情，赢感情牌....");
    }
}
