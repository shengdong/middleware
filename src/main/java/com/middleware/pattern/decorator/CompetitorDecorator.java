package com.middleware.pattern.decorator;

/**
 * Created by Idea
 * date: 2017/7/26 0026.
 * time: 13:49.
 * user: s.he
 */
public class CompetitorDecorator extends Competitor{
    private Competitor competitor;

    public CompetitorDecorator(Competitor competitor) {
        this.competitor = competitor;
    }

    @Override
    public void sing() {
       competitor.sing();
    }
}
