package com.middleware.nio;

import org.apache.log4j.Logger;
import org.junit.Test;

import java.nio.IntBuffer;

/**
 * Created by s.he
 * on 2017\8\22 0022.
 */
public class BufferTest {
    private static final Logger logger = Logger.getLogger(BufferTest.class);

    @Test
    public void testPosition() throws Exception {
        IntBuffer buffer = IntBuffer.allocate(2);
        buffer.put(4);
        buffer.put(5);
        logger.info(buffer.position());
        buffer.flip();

        logger.info(buffer.position());
        logger.info(buffer.get());
        logger.info(buffer.get());
        logger.info(buffer.position());

        buffer.clear();
        logger.info(buffer.position());
    }

    @Test
    public void testLimitCapacity() throws Exception {
        IntBuffer buffer = IntBuffer.allocate(30);
        logger.info(buffer.limit());
        logger.info(buffer.capacity());
        buffer = (IntBuffer) buffer.limit(10);
        logger.info(buffer.limit());
        logger.info(buffer.capacity());
    }

    @Test
    public void testRewind() throws Exception {
        IntBuffer buffer = IntBuffer.allocate(2);
        buffer.put(20);
        buffer.put(120);
        logger.info(buffer.position());
        buffer.rewind();
        logger.info(buffer.position());
        buffer.put(360);
        buffer.put(720);

        buffer.flip();
        logger.info(buffer.get());
        logger.info(buffer.get());

        buffer.rewind();
        logger.info(buffer.get(1));
    }

    @Test
    public void testMark() throws Exception {
        IntBuffer buffer = IntBuffer.allocate(3);
        buffer.put(8 << 2);
        buffer.put(8 << 3);
        buffer.put(8 << 4);
        buffer.flip();

        logger.info(buffer.get());
        logger.info(buffer.position());
        buffer.mark();
        logger.info(buffer.get());
        buffer.reset();
        logger.info(buffer.get());
    }
}
