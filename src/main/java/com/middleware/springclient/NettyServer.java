package com.middleware.springclient;

import io.netty.handler.codec.string.StringEncoder;
import org.apache.log4j.Logger;

import com.middleware.util.ConfigUtil;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;

public class NettyServer {
	private static final Logger logger = Logger.getLogger(NettyServer.class);
	
	
	public void bind(String host, int port) {
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workGroup = new NioEventLoopGroup();
		
		try {
			ServerBootstrap bootstrap = new ServerBootstrap();
			bootstrap.group(bossGroup, workGroup);
			bootstrap.channel(NioServerSocketChannel.class);
			bootstrap.option(ChannelOption.SO_BACKLOG, 100);

			bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {

				@Override
				protected void initChannel(SocketChannel channel) throws Exception {
					String delimiter = ConfigUtil.getString("server.delimiter");
					ByteBuf delimiterBuf = Unpooled.copiedBuffer(delimiter.getBytes());
					channel.pipeline().addLast(new DelimiterBasedFrameDecoder(Integer.MAX_VALUE, delimiterBuf));
					channel.pipeline().addLast(new StringEncoder());
					channel.pipeline().addLast(new DispatcherHandler());
				}
			});
			
			ChannelFuture future = bootstrap.bind(host, port);
			future.channel().closeFuture().sync();
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			bossGroup.shutdownGracefully();
			workGroup.shutdownGracefully();
		}
	}
	
	public static void main(String[] args) {
		NettyServer nettyServer = new NettyServer();
		int port = ConfigUtil.getInt("server.port");
		String host = ConfigUtil.getString("server.host");
		logger.info("bind port : " + port);
		nettyServer.bind(host, port);
	}
}
