package com.middleware.springclient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.UUID;
import org.apache.log4j.Logger;
import com.middleware.util.ConfigUtil;
import lombok.Getter;
import lombok.Setter;

public abstract class MessageHandler {
	private static final Logger logger = Logger.getLogger(MessageHandler.class);

	@Setter @Getter
	private POJOHandler handler;

	private String message;
	
	@Setter @Getter
	private String fileName;

	
	public String handlerMessage(String message, POJOHandler handler) {
		beforeHanderMessage(handler);
		this.message = message.substring(5);
		this.handler = handler;
		boolean flag_file = saveToFile();
		if(!flag_file) {
			logger.info("save to file faild");
			return "save to file faild";
		}
		boolean flag_db = saveToDB(handler.getFilePath() + File.separator + fileName);
		if(!flag_db) {
			logger.info("save to DB faild");
			return "save to DB faild";
		}
		boolean flag_success_file = saveToSuccessPath();
		if(!flag_success_file) {
			logger.info("save to success path faild");
			return "save to success path faild";
		}
		return "success";
	}
	
	public void beforeHanderMessage(POJOHandler handler) {
		
	}
	
	public boolean saveToSuccessPath() {
		String filePath = handler.getFilePath() + File.separator + this.fileName;
		File file = new File(filePath);
		String successPath = handler.getSuccessFilePath() + File.separator + getToday();
		File successFile = new File(successPath);
		if(!successFile.exists()) {
			successFile.mkdir();
		}
		boolean flag_move = file.renameTo(new File(successPath + File.separator + this.fileName));
		if(flag_move) {
			file.delete();
			return true;
		}
		return false;
	}

	public boolean saveToFile() {
		beforeSaveToFile();
		logger.info("handler : " + handler);
		this.fileName = UUID.randomUUID().toString().substring(0, 8) + handler.getFileType();
		boolean flag = saveToFile(handler.getFilePath() + File.separator + fileName);
		afterSaveToFile();
		return flag;
	}

	private String getToday() {
		return ConfigUtil.getToday("yyyyMMdd");
	}

	public void beforeSaveToFile() {

	}

	public void afterSaveToFile() {

	}
	
	public boolean saveToFile(String fileName) {
		OutputStream stream = null;
		try {
			stream = new FileOutputStream(fileName);
			stream.write(message.getBytes());
			stream.flush();
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		} finally {
			try {
				if(stream != null) {
					stream.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
	
	public abstract boolean saveToDB(String path);
}
