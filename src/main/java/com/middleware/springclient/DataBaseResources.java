package com.middleware.springclient;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.log4j.Logger;
import com.middleware.db.MysqlDBCP;

public class DataBaseResources implements Resources{

	private static final Logger logger = Logger.getLogger(DataBaseResources.class);
	
	
	@Override
	public Map<String, POJOHandler> getBeans(String filePath) {
		Map<String, POJOHandler> result = new ConcurrentHashMap<String, POJOHandler>(2<<4);
		MysqlDBCP dbcp = new MysqlDBCP();
		DataSource source = dbcp.getDataSource();
		QueryRunner runner = new QueryRunner(source);
		try {
			List<Map<String, Object>> lists = runner.query("select * from beanTable", new MapListHandler());
			for(Map<String, Object> map : lists) {
				POJOHandler handler = new POJOHandler();
				String flag = (String)map.get("FLAG");
				String path = (String)map.get("FilePath");
				String fileType = (String)map.get("FILETYPE");
				String successFilePath = (String)map.get("SUCCESSFILEPATH");
				String className = (String)map.get("CLASSNAME");
				handler.setFilePath(path);
				handler.setFileType(fileType);
				handler.setFlag(flag);
				handler.setSuccessFilePath(successFilePath);
				handler.setHandler((MessageHandler)Class.forName(className).newInstance());
				result.put(flag, handler);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}
}
