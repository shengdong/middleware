package com.middleware.springclient;

import java.util.Map;

public interface Resources {

	public Map<String, POJOHandler> getBeans(String filePath);
}
