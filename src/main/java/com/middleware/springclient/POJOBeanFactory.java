package com.middleware.springclient;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.middleware.util.ConfigUtil;

public class POJOBeanFactory {
	private static Map<String, POJOHandler> map = new ConcurrentHashMap<String, POJOHandler>();
	
	
	public static POJOHandler getHandlerByFlag(String flag) {
		if(map == null || map.size() == 0) {
			initBean();
		}
		if(map == null) {
			return null;
		}
		return map.get(flag);
	}
	
	public static void initBean() {
		Resources resources = new XmlResources();
		String fileConfig = ConfigUtil.getString("server.fileConfig");
		map = resources.getBeans(fileConfig);
	}
}
