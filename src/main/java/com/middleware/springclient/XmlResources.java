package com.middleware.springclient;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class XmlResources implements Resources {
	private static final Logger logger = Logger.getLogger(XmlResources.class);

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, POJOHandler> getBeans(String filePath) {
		Map<String, POJOHandler> result = new ConcurrentHashMap<String, POJOHandler>(2 << 4);
		SAXReader reader = new SAXReader();
		try {
			Document document = reader.read(filePath);
			Element root = document.getRootElement();
			List<Element> lists = root.elements();
			for(Element element : lists) {
				POJOHandler handler = new POJOHandler();
				String flag = element.attributeValue("name").trim();
				handler.setFlag(flag);
				String className = element.attributeValue("class").trim();
				MessageHandler ms = (MessageHandler) Class.forName(className).newInstance();
				handler.setHandler(ms);
				List<Element> proLists = element.elements();
				for(Element ele : proLists) {
					String value = ele.attributeValue("name").trim();
					if(StringUtils.equals(value, "filePath")) {
						handler.setFilePath(ele.attributeValue("value").trim());
					}
					if(StringUtils.equals(value, "successFilePath")) {
						handler.setSuccessFilePath(ele.attributeValue("value").trim());
					}
					if(StringUtils.equals(value, "fileType")) {
						handler.setFileType(ele.attributeValue("value").trim());
					}
				}
				result.put(flag, handler);
			}
			return result;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		} 
	}
}
