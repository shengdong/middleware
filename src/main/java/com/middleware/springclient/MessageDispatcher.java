package com.middleware.springclient;

import org.apache.log4j.Logger;

public class MessageDispatcher {

	private static final Logger logger = Logger.getLogger(MessageDispatcher.class);
	
	private MessageHandler handler;
	
	
	public String handlerMessage(String message) {
		String flag = message.substring(0, 4);
		POJOHandler pojoHandler = POJOBeanFactory.getHandlerByFlag(flag);
		handler = pojoHandler.getHandler();
		if(handler == null) {
			return "please check message hander, don't find you flag";
		}
		logger.info(pojoHandler);
		return handler.handlerMessage(message, pojoHandler);
	}
}
