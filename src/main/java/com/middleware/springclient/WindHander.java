package com.middleware.springclient;

import org.apache.log4j.Logger;

public class WindHander extends MessageHandler {
	private static final Logger logger = Logger.getLogger(WindHander.class);

	@Override
	public boolean saveToDB(String path) {
		logger.info(path);
		return true;
	}

}
