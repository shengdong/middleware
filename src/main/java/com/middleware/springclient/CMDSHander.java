package com.middleware.springclient;

import org.apache.log4j.Logger;

public class CMDSHander extends MessageHandler {
	private static final Logger logger = Logger.getLogger(CMDSHander.class);
	

	@Override
	public void beforeHanderMessage(POJOHandler handler) {
		super.beforeHanderMessage(handler);
	}

	@Override
	public boolean saveToDB(String path) {
		logger.info(path);
		return true;
	}

}
