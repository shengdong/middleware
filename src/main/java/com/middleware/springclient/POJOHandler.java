package com.middleware.springclient;


public class POJOHandler {

	private String flag;

	private String filePath;

	private String fileType;

	private String successFilePath;

	private MessageHandler handler;

	public POJOHandler() {

	}

	@Override
	public String toString() {
		return "[flag :" + flag + "], [filePath:" + filePath + "], [fileType:" + fileType + "], [successPath:"
				+ successFilePath + "], [handler :" + handler.getClass().getName() + "]";
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getSuccessFilePath() {
		return successFilePath;
	}

	public void setSuccessFilePath(String successFilePath) {
		this.successFilePath = successFilePath;
	}

	public MessageHandler getHandler() {
		return handler;
	}

	public void setHandler(MessageHandler handler) {
		this.handler = handler;
	}
	
	
}
