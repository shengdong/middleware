package com.middleware.springclient;

import io.netty.channel.SimpleChannelInboundHandler;
import org.apache.log4j.Logger;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;

public class DispatcherHandler extends SimpleChannelInboundHandler<Object> {
	private static final Logger logger = Logger.getLogger(DispatcherHandler.class);

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		super.channelRead(ctx, msg);
		String message = (String)msg;
		logger.info("message:" + message);
		
		MessageDispatcher dispatcher = new MessageDispatcher();
		String result = dispatcher.handlerMessage(message);
		ByteBuf echo = Unpooled.copiedBuffer(result.getBytes());
		ctx.writeAndFlush(echo);
		ctx.close();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		super.exceptionCaught(ctx, cause);
		logger.error(cause.getMessage());
		ctx.close();
	}

	@Override
	protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {

	}
}
